admin_OAT = function(ope) {
  pacman::p_load(tidyverse, xlsx, purrr, rio, readxl)

  dir.create(ope)

  chemin= paste("Fichiers/","Identifiants_", ope, ".xlsx", sep = "")
  file.copy(from = chemin,to=ope,overwrite = TRUE)
  path=paste("Identifiants_", ope, ".xlsx", sep = "")

  setwd(ope)

  toto=read_excel(path = path , sheet = "Etabs") %>%
    distinct(
      UAI,
      sequence = Seq,
      Identifiant,
      Mot_de_Passe
    )

  dir.create("Fichiers OAT")
  setwd(dir="Fichiers OAT")

  toto%>%
    write.table(
      paste(ope,"_Administrateurs_test",
            ".csv",
            sep = ""
      ),
      row.names = FALSE,
      quote = FALSE,
      sep = ";",
      fileEncoding = "UTF-8"
    )

  setwd("../../")

}



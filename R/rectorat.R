rectorat=function(){
  
  pacman::p_load(data.table)
  
  ACCES_BCE()  %>%
    filter(categorie_juridique=="101") %>% 
    collect() %>% 
    select(numero_uai,denomination_principale_uai,patronyme_uai,base_gestion,departement,categorie_juridique) %>% 
    filter((denomination_principale_uai %in% grep(denomination_principale_uai,pattern = "RECTORAT",value = TRUE) |
              denomination_principale_uai %in% grep(denomination_principale_uai,pattern = "SERVICE DE L'EDUCATION NAT.",value = TRUE))
           & numero_uai!="9720459S") %>%
    select(-categorie_juridique) %>% 
    return()
}

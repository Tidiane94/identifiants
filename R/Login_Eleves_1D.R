Login_Eleves_1D <- function(Echantillon, Mots) {
  
  pacman::p_load(splitstackshape)
  MOTS = Mots[1:nrow(Echantillon)] %>% as.data.frame() %>% `names<-`("MOT")
  
  
  Logins_Eleves=Echantillon %>% 
    select(UAI,effectif) %>% 
    bind_cols(MOTS) %>% 
    select(UAI,effectif,MOT) %>% 
    expandRows("effectif") %>% 
    split(.$UAI) %>% 
    map(.f = ~INDEX_SANS_ZERO(input = .,n = 3)) %>% 
    bind_rows() %>%
    mutate(Identifiant = str_trim(str_to_upper(paste(MOT,INDEX,sep="")))) %>% 
   rename(Index=INDEX) %>% 
    mutate(Identifiant_Reserve = paste(Identifiant, "R", sep = "")) %>% 
    mutate(sequence=1)
  
  
  bind_rows(
    Logins_Eleves %>% select(-Identifiant_Reserve) %>% mutate(Reserve = 0),
    Logins_Eleves %>% select(-Identifiant) %>%
      rename(Identifiant = Identifiant_Reserve) %>% mutate(Reserve = 1)
  ) %>%
    return()
}


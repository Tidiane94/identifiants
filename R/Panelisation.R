Panelisation <- function(Echantillon) {
  Echantillon_Panel = readline("Echantillon ou Panel? E/P\t")
  while (!Echantillon_Panel %in% c("E", "P"))
  {
    Echantillon_Panel = readline("Echantillon ou Panel? E/P\t")
  }
  if(Echantillon_Panel=="E")
  {
    Fichier_eleves =
      Echantillon2Panel(Echantillon = Echantillon,
                        UAI = "UAI",
                        effectif = "effectif") %>%
      mutate(Index = INDEXATION_NumEleve(Index))
  }
  
  if (Echantillon_Panel == "P")
  {
    Fichier_eleves =
      Echantillon %>%
      group_by(UAI, classe) %>%
      mutate(Index = dense_rank(paste(Prenom, Nom, Naissance))) %>% ungroup() %>%
      mutate(Index = INDEXATION_NumEleve(Index))
  }
  return(Fichier_eleves)
}

depp_VRETTA=function(ope){
  
  pacman::p_load(tidyverse, xlsx, purrr, rio, readxl)
  
  
  Presence_module=readline(prompt = "Existe t-il des modules dans l'évaluation ? oui/non : \t")
  while (!Presence_module %in% c("oui", "non"))
  {
    Presence_module=readline(prompt = "Existe t-il des modules dans l'évaluation ? oui/non : \t")
  }
  if (Presence_module == "non")
  {
    dir.create(ope)
    
    chemin= paste("Fichiers/","Identifiants_", ope, ".xlsx", sep = "")
    file.copy(from = chemin,to=ope,overwrite = TRUE)
    path=paste(ope,"/Identifiants_", ope, ".xlsx", sep = "")
    
    
    # setwd(ope)
    
    onglet = "DEPP"
    
    UAI_rec=rectorat()
    
    UAI_rec$patronyme_uai= str_remove_all(string=UAI_rec$patronyme_uai,pattern="ACADEMIE") 
    UAI_rec$patronyme_uai= str_remove_all(string=UAI_rec$patronyme_uai,pattern="DE LA") 
    UAI_rec$patronyme_uai= str_remove_all(string=UAI_rec$patronyme_uai,pattern="DE ") 
    
  UAI_rec$patronyme_uai=str_remove_all(UAI_rec$patronyme_uai,pattern = "ACADEMIE ")
UAI_rec$patronyme_uai=str_remove_all(UAI_rec$patronyme_uai,pattern = "D'")

  UAI_rec$patronyme_uai=str_replace(UAI_rec$patronyme_uai," ","")

    
    BCE=ACCES_BCE()
    
    toto= read_excel(path = path , sheet = "DEPP") %>%
      mutate(sequence=1) %>% 
      select(
        UAI,
        sequence,
        Id=Identifiant,
        Mdp=Mot_de_Passe
      ) %>% 
      left_join(BCE %>% distinct(base_gestion,numero_uai,denomination_principale_uai,patronyme_uai) %>%
                  mutate(Nom_de_l_ecole=paste(denomination_principale_uai,patronyme_uai,sep=" ")) %>%
                  select(-patronyme_uai,-denomination_principale_uai),by=c("UAI"="numero_uai"),
                copy=TRUE) %>% 
      left_join(UAI_rec %>% 
                  select(UAI_Academie=numero_uai,
                         Nom_de_l_academie=patronyme_uai,
                         base_gestion),by=c("base_gestion")) %>% 
      select(-base_gestion) %>% 
      mutate(statut=onglet) %>% 
      select(UAI_Academie,UAI_Ecole=UAI,Nom_de_l_academie,Nom_de_l_ecole,everything()) %>% 
      mutate(module="")
    
    return(toto)
  }
  if (Presence_module == "oui")
  {
    dir.create(ope)
    
    chemin= paste("Fichiers/","Identifiants_", ope, ".xlsx", sep = "")
    file.copy(from = chemin,to=ope,overwrite = TRUE)
    path=paste(ope,"/Identifiants_", ope, ".xlsx", sep = "")
    
    
    # setwd(ope)
    
    onglet = "DEPP"
    
    UAI_rec=rectorat()
    
UAI_rec$patronyme_uai= str_remove_all(string=UAI_rec$patronyme_uai,pattern="ACADEMIE") 
    UAI_rec$patronyme_uai= str_remove_all(string=UAI_rec$patronyme_uai,pattern="DE LA") 
    UAI_rec$patronyme_uai= str_remove_all(string=UAI_rec$patronyme_uai,pattern="DE ") 
    
  UAI_rec$patronyme_uai=str_remove_all(UAI_rec$patronyme_uai,pattern = "ACADEMIE ")
UAI_rec$patronyme_uai=str_remove_all(UAI_rec$patronyme_uai,pattern = "D'")

  UAI_rec$patronyme_uai=str_replace(UAI_rec$patronyme_uai," ","")

    
    BCE=ACCES_BCE()
    
    toto=read_excel(path = path , sheet = "DEPP") %>%
      mutate(sequence=1) %>% 
      select(
        UAI,
        sequence,
        Id=Identifiant,
        Mdp=Mot_de_Passe
      ) %>% 
      left_join(BCE %>% distinct(base_gestion,numero_uai,denomination_principale_uai,patronyme_uai) %>%
                  mutate(Nom_de_l_ecole=paste(denomination_principale_uai,patronyme_uai,sep=" ")) %>%
                  select(-patronyme_uai,-denomination_principale_uai),by=c("UAI"="numero_uai"),
                copy=TRUE) %>% 
      left_join(UAI_rec %>% 
                  select(UAI_Academie=numero_uai,
                         Nom_de_l_academie=patronyme_uai,
                         base_gestion),by=c("base_gestion")) %>% 
      select(-base_gestion) %>% 
      mutate(statut=onglet) %>% 
      select(UAI_Academie,UAI_Ecole=UAI,Nom_de_l_academie,Nom_de_l_ecole,everything()) %>% 
      mutate(module="")
    
    return(toto)
  }
  
  
  
  
}
FICHIERS_2D <- function(Fichier_eleves, Echantillon, Degre) {
  # Constitution des logins eleves
  Logins_Eleves = Login_Eleves_2D(fichier = Fichier_eleves)
  
  # Constitution des login Admin
  Logins_Admin =
    Fichiers_ADMIN2(Etabs = Fichier_eleves$UAI %>% unique(),
                    fichier = Echantillon,
                    Degre = Degre)
  # Tous les mots de passe
  Fichiers =
    c(list(Logins_Eleves = Logins_Eleves), Logins_Admin) %>%
    map(.f = ~PASSWD(
      input = .,
      longueur = 8,
      #caracteres = "[[0-9a-zA-Z!%$?@#]-[O0lI]]",
        caracteres = "[[0-9a-zA-Z]-[O0lI]]"
    ))
  return(Fichiers)
}